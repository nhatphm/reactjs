var config = {
  entry: './main.js', // file main app

  output: {
    path: '/assets',
    filename: 'bundle.js', // output filename
    publicPath: '/' // path in browser
  },

  devServer: {
    port: 3000,  // port 
    historyApiFallback: true
  },

  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        include: '/',
        loader: 'babel-loader',
        query: {
          presets: ['es2015', 'react'] // es5
        }
      },
      {
        test: /\.json$/,
        exclude: /node_modules/,
        loader: 'json-loader'
      },
      {
        test: /\.html?$/,
        loader: 'html'
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader!autoprefixer-loader'
      },
      {
        test: /\.scss$/,
        loader: 'style-loader!css-loader!autoprefixer-loader!sass-loader'
      }
    ]
  }
}

module.exports = config;