import FaBeer from 'react-icons/lib/fa/beer';
import MdToday from 'react-icons/lib/md/today';
import MdAcUnit from 'react-icons/lib/md/ac-unit';
import React, { PropTypes } from 'react';

const SkiDayItem = ({ resort, date, powder, backcountry }) => (
  <tr>
    <td>
      {date.getMonth() + 1}/{date.getDate()}/{date.getFullYear()}
    </td>
    <td>{resort}</td>
    <td>{(powder) ? <FaBeer /> : null}</td>
    <td>{(backcountry) ? <MdAcUnit /> : null}</td>
  </tr>
)

SkiDayItem.defaultProps = {
  resort: 'empty',
}
SkiDayItem.propTypes= {
  resort: PropTypes.string,
  date: PropTypes.instanceOf(Date).isRequired,
  powder: PropTypes.bool,
  backcountry: PropTypes.bool
}
export default SkiDayItem;