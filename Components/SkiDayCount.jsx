import React, { Component } from 'react';
import FaBeer from 'react-icons/lib/fa/beer';
import MdToday from 'react-icons/lib/md/today';
import MdAcUnit from 'react-icons/lib/md/ac-unit';
import PropTypes from 'prop-types';
const percentToDecimal = (decimal) => {
  return ((decimal * 100) + '%');
}
const calcGoalProgress = (total, goal) => {
  return percentToDecimal(total / goal);
}
const SkiDayCount = (props) =>(
  <div className="ski-day-count">
    <div className="row">
      <div className="col-md-12">
        <div className="total-days text-center">
          <span>{props.total} days</span>
          <MdToday />
        </div>
      </div>
    </div>
    <div className="row">
      <div className="col-md-6 col-xs-6"><div className="powder-days text-center">
        <span>{props.powder} days</span>
        <MdToday />
      </div></div>
      <div className="col-md-6 col-xs-6"><div className="backcountry-days text-center">
        <span>{props.backcountry} hiking day</span>
        <MdAcUnit />
      </div></div>
    </div>
    <div>
      <span>{calcGoalProgress(props.total, props)}</span>
    </div>

  </div >
)
SkiDayCount.propTypes={
  total: PropTypes.number.isRequired
}
export default SkiDayCount;