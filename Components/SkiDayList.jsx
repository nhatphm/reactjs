import FaBeer from 'react-icons/lib/fa/beer';
import MdToday from 'react-icons/lib/md/today';
import MdAcUnit from 'react-icons/lib/md/ac-unit';
import SkiDayItem from './SkiDayItem.jsx';
import React, { PropTypes } from 'react';
import { Link } from 'react-router-dom';

const SkiDayList = ({ days, filter }) => {
  const filterdDays = (!filter ||
    !filter.match(/powder|backcountry/)) ?
    days :
    days.filter(day => day[filter]);
  return (
    <table>
      <thead>
        <tr>
          <th>Date</th>
          <th>Resort</th>
          <th>Powder</th>
          <th>BackCountry</th>
        </tr>
        <tr>
          <th> <Link to="/list-days">All Days</Link></th>
          <th> <Link to="/list-days/powder">Powder Days</Link></th>
          <th> <Link to="/list-days/backcountry">BackCountry Days</Link></th>
        </tr>
      </thead>
      <tbody>
        {filterdDays.map((day, i) =>
          <SkiDayItem key={i} {...day} />
        )}
      </tbody>
    </table>
  )
}
SkiDayList.propTypes = {
  days: PropTypes.array
}
export default SkiDayList;