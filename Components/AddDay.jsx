import React, { Component, PropTypes } from 'react';

export default class AddDay extends Component {
  constructor(props){
    super(props);
    this.submitDay = this.submitDay.bind(this);
  }
  submitDay(e) {
    e.preventDefault();
    console.log(this.refs.backcountry.value);
  }
  render() {
    const { resort, date, powder, backcountry } = this.props;
    return (
      <form className="add-day-form form" onSubmit={this.submitDay.bind(this)}>
        <div className="form-group">
          <label htmlFor="resort" className="col-sm-2 control-label">Resort</label>
          <div className="col-sm-10">
            <input ref="resort" type="text" name="resort" id="resort" className="form-control" defaultValue={resort} />
          </div>
          <label htmlFor="date" className="col-sm-2 control-label">Date</label>
          <div className="col-sm-10">
            <input ref="date" type="date" name="date" id="date" className="form-control" defaultValue={date} />
          </div>
          <label htmlFor="powder" className="col-sm-2 control-label">Powder</label>
          <div className="col-sm-10">
            <input ref="powder" type="checkbox" name="powder" id="powder" defaultChecked={powder} />
          </div>
          <div className="clearfix"></div>
          <label htmlFor="backcountry" className="col-sm-2 control-label">Backcountry</label>
          <div className="col-sm-10">
            <input ref="backcountry" type="checkbox" name="backcountry" id="backcountry" defaultChecked={backcountry} />
          </div>
          <div className="col-sm-10">
            <button type="submit" className="btn btn-primary pull-right">Add day</button>
          </div>
        </div>
      </form>
    )
  }
}
AddDay.defaultProps = {
  resort: 'Halo',
  date: '2017-03-03',
  powder: true,
  backcountry: true
}
AddDay.propTypes = {
  resort: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  powder: PropTypes.bool.isRequired,
  backcountry: PropTypes.bool.isRequired
}